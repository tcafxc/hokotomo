from django.urls import path
from .views import AuthorViewSet, BookViewSet

urlpatterns = [
    path('authors/', AuthorViewSet.as_view({'get': 'list'}), name='authors'),
    path('books/', BookViewSet.as_view({'get': 'list'}), name='books'),
]
