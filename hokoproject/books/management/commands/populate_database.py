from django.conf import settings
from django.core.management.base import BaseCommand
import json
from books.utils import populate_database
from books.models import Book


class Command(BaseCommand):
    help = 'Populate DB with Sample Data'

    def handle(self, *args, **options):
        if not Book.objects.exists():
            with open(settings.BOOKS_DATA_FILE, 'r') as fp:
                sample_data = json.load(fp)
                populate_database(data=sample_data.get('books'))

