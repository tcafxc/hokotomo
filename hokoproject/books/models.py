from django.db import models
import uuid


class Book(models.Model):
    """
    Store Book Data
    """
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    cover = models.URLField()
    isbn = models.TextField()  # TODO which field type is best?
    title = models.TextField()
    subtitle = models.TextField()
    author = models.TextField()
    published = models.DateTimeField()
    publisher = models.TextField(blank=True)
    pages = models.IntegerField(blank=True, null=True)
    description = models.TextField(blank=True)
    website = models.URLField(blank=True)

    def __str__(self):
        return self.title
