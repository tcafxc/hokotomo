from collections import defaultdict
from rest_framework import serializers
from .models import Book


class BookListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = '__all__'


class AuthorBookListSerializer(serializers.ListSerializer):
    book_serializer = BookListSerializer

    def to_representation(self, data):
        formatted_author_data = []
        author_data = defaultdict(list)
        for book in data:
            serialized_book = self.book_serializer(book)
            author_data[book.author].append(serialized_book.data)

        # Restructure the author dictionary to a list of authors and their collection of books.
        for author, books in author_data.items():
            formatted_author_data.append({
                author: books
            })
        return formatted_author_data


class AuthorListSerializer(serializers.ModelSerializer):

    class Meta:
        model = Book
        fields = '__all__'
        list_serializer_class = AuthorBookListSerializer
