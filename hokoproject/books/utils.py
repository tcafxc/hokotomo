from books.serializers import BookListSerializer


def populate_database(data=None):
    for book in data:
        instance = BookListSerializer(data=book)
        if instance.is_valid():
            instance.save()
