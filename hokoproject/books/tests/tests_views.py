from dateutil.parser import parse
from django.conf import settings
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from ..utils import populate_database
import json


class BookTests(APITestCase):
    def setUp(self):
        with open(settings.BOOKS_DATA_FILE, 'r') as fp:
            sample_data = json.load(fp)
            self.books = sample_data.get('books')
            populate_database(data=self.books)

    def test_get_books(self):
        """
        Ensure we can fetch a list of books
        """
        url = reverse('books')
        response = self.client.get(url, format='json')
        books = response.data.get('results')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(books), len(self.books))

    def test_post_books(self):
        """
        Ensure a post call to api/v1/books is NOT allowed
        """

        url = reverse('books')
        response = self.client.post(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_sort_books_by_title_ascending(self):
        """
        Ensure the endpoint /books?ordering=title returns:
        a collection of books, ordered by title in ascending order
        """
        ordering = 'title'
        response = self.create_response_object(url_name='books', ordering=ordering)

        returned_book_titles, expected_book_titles = self.extract_expected_and_returned_data(
            ordering=ordering,
            data=response.data.get('results')
        )

        expected_book_titles.sort()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(returned_book_titles, expected_book_titles)

    def test_sort_books_by_title_descending(self):
        """
        Ensure the endpoint /books?ordering=-title returns:
        a collection of books, ordered by title in descending order
        """
        ordering = '-title'
        response = self.create_response_object(url_name='books', ordering=ordering)

        returned_book_titles, expected_book_titles = self.extract_expected_and_returned_data(
            ordering=ordering,
            data=response.data.get('results')
        )

        expected_book_titles.sort(reverse=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(returned_book_titles, expected_book_titles)

    def test_sort_books_by_published_date_ascending(self):
        """
        Ensure the endpoint /books?ordering=published returns:
        a collection of books, ordered by published date in ascending order
        """
        ordering = 'published'
        response = self.create_response_object(url_name='books', ordering=ordering)

        returned_book_published_dates, expected_book_published_dates = self.extract_expected_and_returned_data(
            ordering=ordering,
            data=response.data.get('results')
        )

        expected_book_published_dates.sort()

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(returned_book_published_dates, expected_book_published_dates)

    def test_sort_books_by_title_published_date_descending(self):
        """
        Ensure the endpoint /books?ordering=-published returns:
        a collection of books, ordered by published date in descending order
        """
        ordering = '-published'
        response = self.create_response_object(url_name='books', ordering=ordering)

        returned_book_published_dates, expected_book_published_dates = self.extract_expected_and_returned_data(
            ordering=ordering,
            data=response.data.get('results')
        )
        expected_book_published_dates.sort(reverse=True)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(returned_book_published_dates, expected_book_published_dates)

    def test_get_authors(self):
        """
        Ensure the endpoint /authors returns:
        a collection of authors, and a list of their books
        """
        with self.assertNumQueries(1):

            url = reverse('authors')
            response = self.client.get(url, format='json')
            authors = response.data.get('results')

            returned_authors = {list(author.keys())[0] for author in authors}
            returned_number_of_authors = len(authors)

            expected_authors = {book.get('author') for book in self.books}
            expected_number_of_authors = len(expected_authors)

            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(returned_number_of_authors, expected_number_of_authors)
            self.assertEqual(returned_authors, expected_authors)

    def test_entity_not_found(self):
        """
        Ensure a 404 is returned:
        when the user accesses a non-existent endpoint
        """
        url = '/url_not_found'
        response = self.client.get(url, format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def create_response_object(self, url_name=None, ordering=None):
        return self.client.get(reverse(url_name), {'ordering': ordering}, format='json')

    def extract_expected_and_returned_data(self, ordering=None, data=None):
        if 'title' in ordering:
            returned_book_data = [book.get('title') for book in data]
            expected_book_data = [book.get('title') for book in self.books]
        else:
            returned_book_data = [parse(book.get('published')).timestamp() for book in data]
            expected_book_data = [parse(book.get('published')).timestamp() for book in self.books]

        return returned_book_data, expected_book_data
