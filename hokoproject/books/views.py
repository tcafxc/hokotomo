from django.http import HttpResponseNotFound
import json
from .models import Book
from .serializers import AuthorListSerializer, BookListSerializer
from rest_framework import filters, viewsets, status


class BookViewSet(viewsets.ReadOnlyModelViewSet):
    """

    This endpoint returns a list of books.

        Sort by title (ascending): /api/v1/books?ordering=title
        Sort by publish date (ascending): /api/v1/books?ordering=published
        Sort by title (descending): /api/v1/books?ordering=-title
        Sort by publish date (descending): /api/v1/books?ordering=-published

    """
    queryset = Book.objects.all()
    serializer_class = BookListSerializer
    filter_backends = [filters.OrderingFilter]
    ordering_fields = ['title', 'published']
    ordering = ['title']


class AuthorViewSet(viewsets.ReadOnlyModelViewSet):
    """

    This endpoint returns a list of authors and the books they've written.

    """
    queryset = Book.objects.all()
    serializer_class = AuthorListSerializer
    filter_backends = [filters.OrderingFilter]
    ordering = ['author']


def Custom404View(request, exception):
    response_data = {
        'error': 'Whoops! Resource Not found.',
        'status_code': 404
    }
    return HttpResponseNotFound(
        json.dumps(response_data),
        status=status.HTTP_404_NOT_FOUND,
        content_type='application/json'
    )
