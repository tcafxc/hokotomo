# Description
This repo allows users to view a collection of [books](https://hokodo-frontend-interview.netlify.com/data.json), which can be sorted by title and publication date.
Additionally, users can view a collection of authors and their collection of books.

1. To view a collection of books, visit the endpoint below:
    - [/api/v1/books](http://hktc.eu-west-1.elasticbeanstalk.com/api/v1/books)
2. To sort this collection by title:
    - [/api/v1/books?ordering=title](http://hktc.eu-west-1.elasticbeanstalk.com/api/v1/books?ordering=title) (ascending order)
    - [/api/v1/books?ordering=-title](http://hktc.eu-west-1.elasticbeanstalk.com/api/v1/books?ordering=-title) (descending order)
3. To sort this collection by published date:
    - [/api/v1/books?ordering=published](http://hktc.eu-west-1.elasticbeanstalk.com/api/v1/books?ordering=published) (ascending order)
    - [/api/v1/books?ordering=-published](http://hktc.eu-west-1.elasticbeanstalk.com/api/v1/books?ordering=-published) (descending order)
4. To view a collection of authors and their collection of books, visit the endpoint below:
    - [/api/v1/authors](http://hktc.eu-west-1.elasticbeanstalk.com/api/v1/authors)

**Note:** Any endpoint ending with a trailing slash with return a 404.

# Installation
#### Pre-requisites
Make sure you have Python 3.4+, Pip and Virtualenv pre-installed.

#### Build Setup
```sh
$ git clone https://tcafxc@bitbucket.org/tcafxc/hokotomo.git 
$ cd hokotomo/
$ virtualenv venv
$ source venv/bin/activate
$ (venv) pip install -r requirements.txt
$ (venv) cd hokoproject
```
#### Initialize Database
 Set the environment variables as described below in **_Environment Variables_** before running any of the steps below.
```
$ (venv) python manage.py migrate
$ (venv) python manage.py populate_database
```
#### Run
```
$ (venv) python manage.py runserver
```

# Testing
```
$ (venv) python manage.py test books
```

#### Test Coverage
```sh
$ (venv) coverage erase
$ (venv) coverage run --source=. manage.py test books -v 2
$ (venv) coverage html -d coverage-report
$ (venv) open coverage-report/index.html
```
You should see a report as shown below.

![coverage image](https://imgur.com/QYJcuZ3.png)

# Staticfiles
1. To setup your staticfiles for production please follow these [instructions](https://www.caktusgroup.com/blog/2014/11/10/Using-Amazon-S3-to-store-your-Django-sites-static-and-media-files/).
2. Add the environment variables listed below in _**Staticfiles Settings**_.

# Environment Variables

#### Core Django Settings (required)
| Name | Value |
| ------ | ------ |
| DEBUG | False |
| SECRET_KEY |  |
| ALLOWED_HOSTS |  |

#### Database Settings (required)
| Name | Value |
| ------ | ------ |
| DB_NAME |  |
| DB_USER |  |
| DB_PWD |  |
| DB_HOST |  |
| DB_PORT |  |

#### Django Rest Framework Settings (optional)

| Name | Value |
| ------ | ------ |
| ANON_THROTTLE_RATE | 10/hour (default) |
| PAGINATION_SIZE | 10  (default) |

#### Staticfiles Settings (optional)

| Name | Value |
| ------ | ------ |
| AWS_ACCESS_KEY_ID | xxxxxxx |
| AWS_SECRET_ACCESS_KEY | xxxxxxx  |
| AWS_STORAGE_BUCKET_NAME | xxxxxxx |
| AWS_S3_REGION_NAME | xxxxxxx |

# TODO
1. Better documentation.
2. Additional unit testing.
3. Add HTTPS.

4. More readable API URLS (e.g. /books?sort_by=title&order=desc).
5. Investigate better methods to serialize author collection.
6. Add API versioning.
7. Add user authentication and permissions.
8. Select more appropriate Book model fields.


# Assumptions
1. Book ids are **not** important. Currently new ids are generated on save.